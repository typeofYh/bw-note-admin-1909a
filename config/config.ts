/*
 * @Author: Yh
 * @LastEditors: Yh
 * @info: 项目启动时初始配置， webpack或者node编译的时候执行的  config/config.ts 和 umirc.ts 都是配置文件，umirc优先级高
 */
import { defineConfig } from 'umi';

export default defineConfig({
  title: '创作平台后台管理',
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { 
      path: '/login', 
      title: '登录',
      menuRender: false,
      headerRender: false,
      component: '@/pages/login/index' 
    },
    { 
      path: '/registry', 
      title: '注册',
      menuRender: false,
      headerRender: false,
      component: '@/pages/registry/index' 
    },
    {
      path: '/',
      name: '工作台', 
      title:'创作平台后台管理',
      component: '@/pages/index/index' 
    },
    {
      path: '/users',
      name: '用户管理', 
      title:'创作平台后台管理',
      access: 'isAdmin', // 在渲染组件前判断 access.isAdmin值是否为true，为true正常渲染页面，为false返回403页面
      component: '@/pages/users/index' 
    },
    {
      path: '/create/:type', 
      title:'创作平台后台管理',
      menuRender: false,
      headerRender: false,
      component: '@/pages/create/index' 
    },
  ],
  fastRefresh: {},
  layout: {
    name: '创作平台',
    logo: 'https://www.bwie.com/static/home/logo/logo.jpg'
  },
  proxy: {
    '/api': {
      'target': 'https://creationapi.shbwyz.com/',
      'changeOrigin': true
      // 'pathRewrite': { '^/api' : '' },  // 重写路径
    },
  }
});
