const http = require('http');
const path = require('path');
const fs = require('fs');
const axios = require('axios');
const baseUrl = `https://creationapi.shbwyz.com`;
const server = http.createServer(async (req, res) => {
  if(req.url === '/favicon.ico'){
    return res.end('');
  }
  if(req.url === '/'){
    return res.end(fs.readFileSync(path.join(__dirname,'index.html')))
  }
  const filePath = path.join(__dirname,req.url);  // 静态资源
  if(fs.existsSync(filePath)){  // 判断文件是否存在
    return res.end(fs.readFileSync(filePath));
  }
  // 处理history模式404的问题
  if(req.headers['sec-fetch-dest'] === 'document'){ // 前端路由
    return res.end(fs.readFileSync(path.join(__dirname,'index.html'))) // 加载js之后取到window.location.pathname动态渲染页面
  }

  // 解决跨域问题，解决反向代理
  if(/^\/api/.test(req.url)){  // 以api开始的接口
    try{
      const {data} = await axios({ // 服务端帮前端发请求拿数据
        ...req,
        url: `${baseUrl}${req.url}`
      });
      // console.log(data);
      return res.end(JSON.stringify(data));
    }catch(error){
      console.log(error);
    }
  }


  return res.end('404');

})

server.listen(3000,() => {
  console.log('port is 3000');
})