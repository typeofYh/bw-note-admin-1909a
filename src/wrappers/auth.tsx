/*
 * @Author: Yh
 * @LastEditors: Yh
 */
const auth = (props) => {   // 函数返回值就是视图要展示的内容
  return (
    <>
      {props.children}
    </>
  )
}

export default auth;