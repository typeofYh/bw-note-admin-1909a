/*
 * @Author: Yh
 * @LastEditors: Yh
 */
interface Option {
  type?: string;
}
const types = ['localStorage','sessionStorage'];

class Storage {
  _storage: any;
  constructor(option:Option = {type: 'localStorage'}){
    option.type = option.type || 'localStorage';
    if(types.includes(option.type) && window[option.type]){
      this._storage = window[option.type];
    }
  }
  get(key:string){
    let value:string | null = this._storage.getItem(key);
    try {
      if(value) {
        return JSON.parse(value);
      }
      return value;
    } catch (error) {
      throw new Error(error);
    }
  }
  set(key:string, value:any){
    try {
      this._storage.setItem(key, JSON.stringify(value));
    } catch(error){
      throw new Error(error);
    }
  }
  remove(key:string | undefined){
    // remove 不传key删除全部， 传key删除单个
    if(key){
      this._storage.removeItem(key);
    } else {
      this._storage.clear();
    }
  }
}


export const localStorage = new Storage({
  type: 'localStorage'
})
export const sessionStorage = new Storage({
  type: 'sessionStorage'
})
export default Storage;


// localStorage sessionStorage cookies 区别