/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import BaseChart from "@/components/baseChart"
import { useRequest } from "ahooks"
import { getChartData, getArticleList } from "@/api/index"
import { Card } from 'antd';
import { CSSProperties } from "react";
const gridStyle:CSSProperties = {
  width: '33%',
  textAlign: 'center',
};
const Index = () => {
  const { loading, data } = useRequest(getChartData, {
    pollingInterval: 10000,
  });
  const {loading: articleLoading, data: articleData} = useRequest(getArticleList)
  return (
    <div>
      <BaseChart 
        type="line"
        chartData={{
          title: {
            text: '销售统计图',
            subtext: '周统计图'
          },
          legend: {
            type:'plain'
          }
        }}
        series={[
        {
          type:'line',
          name: '访问量',
          data: data?.data[0]
        },
        {
          type:'bar',
          name: '成交量',
          data: data?.data[1]
        }]}
        xData={['周一','周二','周三','周四','周五']}
        loading={loading}
        width={400}
        height={300}
      />
      {/**文章列表 */}
      <Card 
        title="最新文章"
        loading={articleLoading}
      >
        {
          articleData?.data[0].map((item:any) => (
              <Card.Grid key={item.id} style={gridStyle}>
                {item.title}
              </Card.Grid>
            )
          )
        }
      </Card>
    </div>
  )
}

export default Index;