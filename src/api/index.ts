/*
 * @Author: Yh
 * @LastEditors: Yh
 */
import { request } from "umi"
import { BasePageParams } from "./api.d"
export const getChartData = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve({
      success:true,
      msg:'',
      data: [
        new Array(7).fill('').map(() => Math.floor(Math.random() * 100) + 1),
        new Array(7).fill('').map(() => Math.floor(Math.random() * 100) + 1)
      ]
    })
  },500)
})

export const getArticleList = (params: BasePageParams = {page:1, pageSize: 6}) => request('/api/article', {
  params
})