/*
 * @Author: Yh
 * @LastEditors: Yh
 */
export interface BasePageParams {
  page: number;
  pageSize: number;
}